#pragma once

#include "../../NoSqlDb/DbEngine/DbMain.h"
#include "..\..\NoSqlDb\Persistence\Persist.h"

class TypeTable
{
public:
    using SqlDbType = std::string;
    using StringDb = NoSqlDb<SqlDbType, SqlDbType>;
    using StringRec = Record<SqlDbType, SqlDbType>;

private:
    NoSqlDb<SqlDbType, SqlDbType> TypeTableDb;

public:
    void addType(std::string Name, std::string Type, std::string File);
    void PersistTypeTable(std::string FileName);
    void LoadTypeTable(std::string FileName);
    Persist<SqlDbType, SqlDbType>::SPtr GetRootElement();
    StringDb& GetTypeTable() { return TypeTableDb; }
};
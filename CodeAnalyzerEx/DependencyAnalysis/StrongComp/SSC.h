#pragma once
#include "../Graph/Graph.h"
#include "../DepAnal/DepAnal.h"
#include "../../NoSqlDb/XmlInterface/XmlElement.h"

using namespace CodeAnalysis;
using namespace XmlProcessing;

class StrongConnComp
{
    SSC *ssc;
    DepAnal::StringDb& DepTableRef;

    void buildSSC();
public:
    StrongConnComp(DepAnal::StringDb& aDepTableRef);
    ~StrongConnComp();
    void showSSC();
    AbstractXmlElement::sPtr GetRootElement();
};
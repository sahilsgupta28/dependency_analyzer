#pragma once

#include <iostream>
#include "../../NoSqlDb/DbEngine/DbMain.h"
#include "../../Parser/ActionsAndRules.h"
#include "..\..\NoSqlDb\Persistence\Persist.h"

namespace CodeAnalysis
{
    class DepAnal
    {
    public:
        //using SPtr = std::shared_ptr<ASTNode*>;
        using FileType = std::string;
        using SqlDbType = std::string;
        using StringDb = NoSqlDb<SqlDbType, SqlDbType>;
        using StringRec = Record<SqlDbType, SqlDbType>;
    private:
        AbstrSynTree& ASTref_;
        ScopeStack<ASTNode*> scopeStack_;
        Scanner::Toker& toker_;
        StringDb& TypeTable_;
        StringDb DepAnalTable;
        std::vector<FileType> Files;

        //Member Function
    private:
        //void generateDepAnalTable(ASTNode* pNode);
        void fillDepTable(ASTNode* pNode);
        void formatDepTable();
        void ParseTokens();
        void DepAnal::addToDepTable(const std::string& Parent, const std::string &Child);

    public:
        DepAnal(NoSqlDb<std::string, std::string>& aTypeTable);
        void doDepAnal();
        void dispalyDepAnal();
        StringDb& getDepTable();
        void setFiles(const std::vector<FileType> files);
        Persist<SqlDbType, SqlDbType>::SPtr GetRootElement();
    };
}
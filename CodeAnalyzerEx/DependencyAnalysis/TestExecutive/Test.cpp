/*
 * Command Line Args:
 * - path: possibly relative path to folder containing all analyzed code,
 *         e.g., may be anywhere in the directory tree rooted at that path
 * - patterns: one or more file patterns of the form *.h, *.cpp, and *.cs
 * - XML Path [optional]: path to store XML file 
 */

#include <iostream>
#include <vector>


#include "..\TypeAnal\TypeAnal.h"
#include "..\DepAnal\DepAnal.h"
#include "..\StrongComp\SSC.h"
#include "..\Display\Display.h"
#include "..\..\Analyzer\Executive.h"
#include "..\..\NoSqlDb\Persistence\Persist.h"

using namespace std;
using namespace CodeAnalysis;

int main(int argc, char* argv[])
{
    string XMLPath = "..\\..\\Result.xml";
    int UpdatedArgc = argc;
    if (argc < 2) {
        cout << "Invalid Args";
    }

    if (argv[argc - 2][0] == '/' && argv[argc - 2][1] == 'x') {
        XMLPath = argv[argc - 1];
        cout << "\n[REQUIREMENT 8] Processing command line argument [XML Path] : " << XMLPath;
        UpdatedArgc -= 2;
    }
    else {
        cout << "\n[REQUIREMENT 8] Using Default XML Path : " << XMLPath;
    }

    Display d("Type_Based_Dependency_Analysis");

    cout << "\nAnalyzing Code...";
    CodeAnalysisExecutive c;
    c.DoCodeAnal(UpdatedArgc, argv);
    
    cout << "\n[REQUIREMENT 4] Type Analysis & Type Table";
    TypeAnal ta;
    ta.doTypeAnal();
    d.addNewComponent(ta.GetRootElement());
    d.displayTypeTable(ta.GetTypeTable());

    cout << "\n[REQUIREMENT 5] Dependency Analysis";
    DepAnal da(ta.GetTypeTable());
    da.setFiles(c.getAllFiles());
    da.doDepAnal();
    d.addNewComponent(da.GetRootElement());

    cout << "\n[REQUIREMENT 6] Strong Component Analysis";
    StrongConnComp ssc(da.getDepTable());
    ssc.showSSC();
    d.addNewComponent(ssc.GetRootElement());

    cout << "\n[REQUIREMENT 7] Writing Results to XML file @ " << XMLPath <<"\n";
    d.save(XMLPath);
    cout << d.ToXmlString();
}
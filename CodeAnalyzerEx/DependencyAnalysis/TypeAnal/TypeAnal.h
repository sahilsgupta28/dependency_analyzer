#pragma once
/////////////////////////////////////////////////////////////////////////
// TypeAnal.h - Demonstrate how to start developing type analyzer      //
//                                                                     //
// Jim Fawcett, CSE687 - Object Oriented Design, Spring 2017           //
/////////////////////////////////////////////////////////////////////////
/*
 * You need to provide all the manual and maintenance informnation
 */

#include <iostream>
#include <functional>
#include "../TypeTable/TypeTable.h"
#include "../../Parser/ActionsAndRules.h"
#include "../../AbstractSyntaxTree/AbstrSynTree.h"
#include "..\..\NoSqlDb\Persistence\Persist.h"

namespace CodeAnalysis
{
    class TypeAnal
    {
        //Data Members
    private:
        AbstrSynTree& ASTref_;
        ScopeStack<ASTNode*> scopeStack_;
        Scanner::Toker& toker_;
        TypeTable types;

        //Member Functions
    private:
        void generateTypeTable(ASTNode* pNode);
        void AnalyzeVariableDeclaration(DeclarationNode pDecl, std::string NodeName);

    public:
        TypeAnal();
        void doTypeAnal();

        TypeTable::StringDb& GetTypeTable() { return types.GetTypeTable(); }
        void PersistTypeTable(std::string FileName) { types.PersistTypeTable(FileName); }
        Persist<TypeTable::SqlDbType, TypeTable::SqlDbType>::SPtr GetRootElement() { return types.GetRootElement(); }
    };
}
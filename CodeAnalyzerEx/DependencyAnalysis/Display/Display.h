#pragma once

#include "../../NoSqlDb/XmlInterface/XmlElement.h"
#include "../../NoSqlDb/DbEngine/DbMain.h"

using namespace std;
using namespace XmlProcessing;

class Display
{
public:
    using Xml = string;
    using SPtr = shared_ptr<AbstractXmlElement>;

private:
    SPtr pRoot;

public:
    Display(string ModuleName);
    void save(std::string FilePath);
    void addNewComponent(SPtr pNode);
    std::string ToXmlString();
    void displayTypeTable(NoSqlDb<std::string, std::string> &db);
    void formatTypeTable(NoSqlDb<std::string, std::string> &TypeTableDb);
};

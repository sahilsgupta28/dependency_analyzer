/**
 * DateTime Test
 * ---------------------
 * Tests functionality of DateTime Class
 *
 * FileName     : DateTime.cpp
 * Author       : Sahil Gupta
 * Date         : 7 February 2017
 * Version      : 1.0
 */

#include <iostream>
#include <string>
#include <ctime>

#include "DateTime.h"
#include "common.h"

DateTimeStruct::DT2_usFormatStr DateTimeStruct::GetCurrentDateTime()
{
    g_common_int;

    time_t t = time(nullptr);
    tm tm;
    localtime_s(&tm, &t);
    std::stringstream ss;
    ss << std::put_time(&tm, "%Y/%d/%m %H:%M:%S");
    return ss.str();
}

/* Enter date in "YYYY/DD/MM HH:MM:SS" in 24-hour format
* Return true if date specified by Value comes after reference date
*/
bool DateTimeStruct::IsOlderThan(const DT2_usFormatStr& Reference, const DT2_usFormatStr& Value)
{
    if (Reference.compare(Value) >= 0)
        return false;

    return true;
}

void DateTimeStruct::setFormat(DT1_FORMAT af)
{
    DT6_publ_enum_format = af;
}

#ifdef __TEST_DATETIME__
int main()
{
    std::cout << DateTimeStruct::GetCurrentDateTime() << std::endl;

    std::string D1 = "2017/08/02 00:18:21";
    std::string D2 = "2017/07/02 00:18:21";
    std::cout << DateTimeStruct::IsOlderThan(D1,D2) << std::endl;
}
#endif